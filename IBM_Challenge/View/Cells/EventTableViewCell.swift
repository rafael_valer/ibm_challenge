//
//  EventTableViewCell.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 26/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    private var event: Event!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var gradientImageView: UIImageView!
    @IBOutlet weak var imgImageView: UIImageView!
    
    static let cellHeight: CGFloat = 190.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        gradientImageView.cornerRadius = 4.0
        gradientImageView.contentMode = .scaleAspectFill
        imgImageView?.cornerRadius = 4.0
        imgImageView.contentMode = .scaleAspectFill
        
        titleLabel.font = UIFont.appTitle
        titleLabel.textColor = .white
        dateLabel.font = UIFont.appBody
        dateLabel.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func render(withEvent event: Event) {
        self.event = event
        
        titleLabel.text = event.title
        dateLabel.text = event.date.getFormattedDate(format: "dd/MM/yy")
        event.getImage({ (image, error) in
            self.imgImageView?.image = image
        })
    }
}
