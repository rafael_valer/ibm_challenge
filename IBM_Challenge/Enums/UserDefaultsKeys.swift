//
//  UserDefaultsKeys.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 28/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation

enum UserDefaultKeys: String {
    case userName = "user_name"
    case userEmail = "user_email"
    case checkedEvents = "checked_events_ids"
}
