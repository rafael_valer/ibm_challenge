//
//  APIClient.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import Alamofire

class APIClient {
    
    var session: Session

    init(session: Session = Session.default) {
        self.session = session
    }
    
    
    @discardableResult
    private func performRequest<T: Decodable>(request: URLRequestConvertible,
                                      decoder: JSONDecoder = JSONDecoder(),
                                      completion: @escaping (T?, Error?) -> Void) -> DataRequest {
        return AF.request(request).responseDecodable(decoder: decoder, completionHandler: { (response: DataResponse<T, AFError>) in
            
            do {
                let result = try  response.result.get()
                completion(result, response.error?.underlyingError)
            } catch let error {
                completion(nil, error)
            }
        })
    }
    
    
    @discardableResult
    private func performRequest<T: Encodable>(request: URLRequestConvertible,
                                      completion: @escaping (T?, Error?, Bool) -> Void) -> DataRequest {
        
        return AF.request(request).response { (response: AFDataResponse<Data?>) in
            if let error = response.error {
                completion(nil, error, false)
            } else {
                completion(nil, nil, true)
            }
        }
    }
    
    /// Requests all the events from API
    /// - Parameter completion:returns the requested events and possible errors
    func getEvents(_ completion: @escaping ([Event]?, Error?) -> Void) {
        performRequest(request: APIRouter.events, completion: completion)
    }
    
    /// Requests a single event from API
    /// - Parameters:
    ///   - id: event id
    ///   - completion: returns the event requested and possible errors
    func getEvent(id: String, _ completion: @escaping(Event?, Error?) -> Void) {
        performRequest(request: APIRouter.getEvent(id), completion: completion)
    }
    
    /// Requests the checki-n operation
    /// - Parameters:
    ///   - checkin: structure to be checked in
    ///   - completion: returns possible errors and a boolean indicating wheter the operation succeeded or not
    func checkIn(checkin: Checkin, _ completion: @escaping(Checkin?, Error?, Bool) -> Void) {
        performRequest(request: APIRouter.checkIn(checkin: checkin), completion: completion)
    }
    
    
    /// Requests the image from url
    /// - Parameters:
    ///   - url: image url
    ///   - completion: possible errors and the image from url
    func getImage(url: URL, _ completion: @escaping (UIImage?, Error?) -> Void) {
        
        AF.request(url ,method: .get).response{ response in

            switch response.result {
                case .success(let responseData):
                    if let responseData = responseData {
                        completion(UIImage(data: responseData, scale:1), nil)
                    } else {
                        completion(nil, nil)
                }
                case .failure(let error):
                    print("Error retrieving image - \(error)")
            }
        }
    }
    
}
