//
//  APIRouter.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import Alamofire


enum APIRouter: URLRequestConvertible {
    
    static let baseURLString = "https://5b840ba5db24a100142dcd8c.mockapi.io/api"
    
    /// events
    case events
    case getEvent(_ id: String)
    /// check-in
    case checkIn(checkin: Checkin)
    
    var method: HTTPMethod {
        switch self {
        case .events, .getEvent(_):
            return .get
        case .checkIn(_):
            return .post
        }
    }

    var path: String {
        switch self {
        case .events:
            return "/events"
        case .getEvent(let eventId):
            return "/events/\(eventId)"
        case .checkIn(_):
            return "/checkin"
        }
    }

    var body: Parameters {
        switch self {
        case .checkIn(let checkin):
            return ["eventId": checkin.eventId, "name": checkin.name, "email": checkin.email]
        default:
            return [:]
        }
    }

    // MARK: URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try APIRouter.baseURLString.asURL()

        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue

        return urlRequest
    }
}
