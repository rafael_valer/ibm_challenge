//
//  MapManager.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 29/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class MapManager {
    
    
    /// return location address name from latitude and longitude
    /// - Parameters:
    ///   - latitude: double latitude value
    ///   - longitude: double longitude value
    ///   - completion: closure response with address name
    static func getLocationName(latitude: Double, longitude: Double, _ completion: @escaping(String?) -> Void) {
        
        // Create Location
        let location = CLLocation(latitude: latitude, longitude: longitude)

        // Geocode Location
        CLGeocoder().reverseGeocodeLocation(location) { (placemarks, error) in
            // Process Response
            if let error = error {
                print("Unable to Reverse Geocode Location (\(error))")
                completion(nil)
            } else {
                if let placemarks = placemarks, let placemark = placemarks.first {
                    completion(placemark.name)
                } else {
                    print("No Matching Addresses Found")
                    completion(nil)
                }
            }
        }
    }
    
    
    /// Helper function to open Maps app with a location
    /// - Parameters:
    ///   - latitude: double latitude value
    ///   - longitude: double longitude value
    static func openMapsWithDestination(latitude: Double, longitude: Double, destinationName: String? = "Destino") {
        
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: latitude, longitude: longitude)))
        destination.name = destinationName

        MKMapItem.openMaps(with: [destination], launchOptions: nil)
    }
    
}
