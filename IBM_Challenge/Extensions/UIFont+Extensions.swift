//
//  UIFont+Extensions.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 27/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    static var appTitle: UIFont {
        return UIFont.boldSystemFont(ofSize: 21)
    }
    static var appBody: UIFont {
        return UIFont.systemFont(ofSize: 16)
    }
    
}
