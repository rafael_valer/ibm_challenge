//
//  UIColor+Extensions.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 27/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    
    /// Convenience init for UIColor
    /// - Parameters:
    ///   - red: Red RGB value
    ///   - green: Green RGB value
    ///   - blue: Blue RGB value
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Red argument out of bounds")
        assert(green >= 0 && green <= 255, "Green argument out of bounds")
        assert(blue >= 0 && blue <= 255, "Blue argument out of bounds")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    static let appBlue = UIColor(red: 0, green: 122, blue: 255)
}
