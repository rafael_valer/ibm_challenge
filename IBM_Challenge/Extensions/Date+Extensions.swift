//
//  Date+Extensions.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 26/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation

extension Date {
    
    /// Helper function to format Date to a readable String
    /// - Parameter format: date format
    /// - Returns: string with date transformed to input format style
    func getFormattedDate(format: String) -> String {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = format
        return dateFormat.string(from: self)
    }
    
    /// Helper function to initialize Date from integer format
    var millisecondsSince1970:Int64 {
        return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
    }

    init(milliseconds:Int64) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds) / 1000)
    }
}
