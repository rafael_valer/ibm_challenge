//
//  Event.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import UIKit

struct Event: Codable {
    
    let people: [Person]
    let date: Date
    let description: String
    let image: URL
    let longitude: Double
    let latitude: Double
    let price: Double
    let title: String
    let id: String
    let coupons: [Coupon]
    
    enum CodingKeys: String, CodingKey {
        case people
        case date
        case description
        case image
        case longitude
        case latitude
        case price
        case title
        case id
        case coupons = "cupons"
    }
}

// MARK: - Init
extension Event {
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        people = try container.decode([Person].self, forKey: .people)
        // converting Date to according type
        let dateInt = try container.decode(Int.self, forKey: .date)
        date = Date(milliseconds: Int64(dateInt))
        description = try container.decode(String.self, forKey: .description)
        image = try container.decode(URL.self, forKey: .image)
        longitude = try container.decode(Double.self, forKey: .longitude)
        latitude = try container.decode(Double.self, forKey: .latitude)
        price = try container.decode(Double.self, forKey: .price)
        title = try container.decode(String.self, forKey: .title)
        id = try container.decode(String.self, forKey: .id)
        coupons = try container.decode([Coupon].self, forKey: .coupons)
    }
}

// MARK: - Methods
extension Event {
    
    /// Method to get the event image.
    /// - Parameter completion: UIImage from url and possible errors
    func getImage(_ completion: @escaping (UIImage?, Error?) -> Void) {
        APIClient().getImage(url: image) { (image, error) in
            completion(image, error)
        }
    }
    
    /// Method to get the shared text for a given event
    /// - Returns: String with event details
    func getShareableText() -> String {
        var text = self.title
        text += "\nDate: " + date.getFormattedDate(format: "dd/MM/yy - HH:mm")
        text += "\n\n----------\n\n"
        text += description
        return text
    }
}


