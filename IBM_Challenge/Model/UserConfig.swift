//
//  UserConfig.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 28/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation

// MARK: User Configurations
/// Caching user basic information to User Defaults
class UserConfig {
    
    // singleton reference to UserConfig
    static let shared = UserConfig()
    private var userDefaults: UserDefaults!
    
    private init() {
        userDefaults = UserDefaults.standard
    }
    
    var name: String {
        get {
            return userDefaults.value(forKey: UserDefaultKeys.userName.rawValue) as? String ?? ""
        }
        set {
            userDefaults.setValue(newValue, forKey: UserDefaultKeys.userName.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var email: String {
        get {
            return userDefaults.value(forKey: UserDefaultKeys.userEmail.rawValue) as? String ?? ""
        }
        set {
            userDefaults.setValue(newValue, forKey: UserDefaultKeys.userEmail.rawValue)
            userDefaults.synchronize()
        }
    }
}

// MARK: - CheckIn Persistence
/// Simulating the persistence of checked events by user, information in a noramal situation would be retrieved by the API
extension UserConfig {
    
    private var checkedEvents: Set<String> {
        get {
            let checkedEvents =  userDefaults.value(forKey: UserDefaultKeys.checkedEvents.rawValue) as? [String] ?? []
            return Set<String>(checkedEvents)
        }
        set {
            let checkedEventsArray = Array(newValue)
            userDefaults.setValue(checkedEventsArray, forKey: UserDefaultKeys.checkedEvents.rawValue)
            userDefaults.synchronize()
        }
    }
    
    func isEventCheked(eventId: String) -> Bool {
        return checkedEvents.contains(eventId)
    }
    
    func checkInEvent(eventId: String) {
        var ids = checkedEvents
        ids.insert(eventId)
        checkedEvents = ids
    }
    
    func uncheckInEvent(eventId: String) {
        var ids = checkedEvents
        ids.remove(eventId)
        checkedEvents = ids
    }
}
