//
//  Coupon.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation

struct Coupon: Codable {
    
    let id: String
    let eventId: String
    let discount: Double
    
    enum CodingKeys: String, CodingKey {
        case id
        case eventId
        case discount
    }
}
