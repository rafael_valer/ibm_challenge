//
//  Person.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation

struct Person: Codable {
    
    let id: String
    let eventId: String
    let name: String
    let picture: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case eventId
        case name
        case picture
    }
}
