//
//  Checkin.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation

struct Checkin: Codable {
    
    let eventId: String
    let name: String
    let email: String
    
    enum CodingKeys: String, CodingKey {
        case eventId
        case name
        case email
    }
}
