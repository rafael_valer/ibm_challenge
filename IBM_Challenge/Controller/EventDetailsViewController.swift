//
//  EventDetailsViewController.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 26/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import UIKit

class EventDetailsViewController: UIViewController {
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkInButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var contentStackView: UIStackView!
    
    // This info should be retrieved from server concerning the list of checkins for this event
    private var isCheckingDone: Bool! {
        didSet {
            checkInButton.isSelected = isCheckingDone
            updateCheckInButtonView()
        }
    }
    
    var event: Event!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        loadWithEvent()
    }
    
    private func setupView() {
        
        title = "Detalhes"
        contentStackView.spacing = 16.0
        navigationItem.largeTitleDisplayMode = .never
        
        // imageViews setup
        headerImageView.clipsToBounds = true
        headerImageView.contentMode = .scaleAspectFill
        locationImageView.tintColor = UIColor.darkGray
        priceImageView.tintColor = UIColor.darkGray
        
        // labels setup
        dateLabel.textColor = UIColor.appBlue
        dateLabel.font = UIFont.appBody
        titleLabel.font = UIFont.appTitle
        titleLabel.numberOfLines = 0
        priceLabel.font = UIFont.appBody
        locationLabel.font = UIFont.appBody
        descriptionTitleLabel.font = UIFont.appTitle
        descriptionLabel.font = UIFont.appBody
        descriptionLabel.numberOfLines = 0
        
        // buttons setup
        shareButton.backgroundColor = UIColor.appBlue
        shareButton.imageView?.tintColor = UIColor.white
        checkInButton.layer.borderWidth = 1
        checkInButton.setTitleColor(.darkGray, for: .normal)
        checkInButton.setTitleColor(.appBlue, for: .selected)
        checkInButton.setTitle("Check In", for: .normal)
        checkInButton.setTitle("Checked In", for: .selected)
        checkInButton.setImage(UIImage(systemName: "star.fill"), for: .selected)
        checkInButton.setImage(UIImage(systemName: "star"), for: .normal)
        checkInButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 12)
        
        // check if event was already checked
        isCheckingDone = UserConfig.shared.isEventCheked(eventId: event.id)
    }
    
    // update check in button layout regarding if already checked in on event
    private func updateCheckInButtonView() {
        if checkInButton.isSelected {
            checkInButton.layer.borderColor = UIColor.appBlue.cgColor
            checkInButton.tintColor = .appBlue
            checkInButton.backgroundColor = UIColor.appBlue.withAlphaComponent(0.3)
        } else {
            checkInButton.layer.borderColor = UIColor.darkGray.cgColor
            checkInButton.tintColor = .darkGray
            checkInButton.backgroundColor = .clear
        }
    }
    
    // Populate view with Event details
    private func loadWithEvent() {
        
        event.getImage { (image, error) in
            self.headerImageView.image = image
        }
        
        dateLabel.text = event.date.getFormattedDate(format: "dd/MM/yy - HH:mm")
        titleLabel.text = event.title
        priceLabel.text = "Preço: R$\(event.price)"
        descriptionTitleLabel.text = "Detalhes"
        descriptionLabel.text = event.description
        locationLabel.text = "Abrir no Mapa"
        
        MapManager.getLocationName(latitude: event.latitude, longitude: event.longitude) { (address: String?) in
            if let address = address {
                self.locationLabel.text = address
            }
        }
    }
}

extension EventDetailsViewController {
    
    // MARK: - Alerts
    /// Present error alert
    private func presentErrorAlert() {
        let alert = AppAlertController(title: "Ocorreu um erro na operação", message: "Tente novamente mais tarde", preferredStyle: .alert)
            .addAction(title: "Ok", style: .cancel)
            .build()
        present(alert, animated: true, completion: nil)
    }
    
    /// Present alert for user to complete information regarding checkin
    private func presentCheckinAlert() {
        let alert = AppAlertController(title: "Por favor, forneça as seguintes informações:", message: nil, preferredStyle: .alert)
            .addTextField(isMandatory: true, { (textField) in
                textField?.placeholder = "Nome"
                textField?.keyboardType = .namePhonePad
                textField?.text = UserConfig.shared.name
            })
            .addTextField(isMandatory: true, { (textField) in
                textField?.placeholder = "Email"
                textField?.keyboardType = .emailAddress
                textField?.text = UserConfig.shared.email
            })
            .addAction(title: "Confirmar", style: .default, isSubmitAction: true, handler: { (_, alertController) in
                guard let textFields = alertController.textFields else {
                    debugPrint("Failed retrieving textfields from AlertController")
                    return
                }

                guard let name = textFields[0].text, let email = textFields[1].text else {
                    debugPrint("Failed retrieving textfields text")
                    return
                }

                // Caching user information
                UserConfig.shared.name = name
                UserConfig.shared.email = email
                
                let checkin = Checkin(eventId: self.event.id, name: name, email: email)
                
                self.isCheckingDone = true
                APIClient().checkIn(checkin: checkin) { (checkin, error, succeed) in
                    if succeed == false {
                        self.isCheckingDone = false
                        self.presentErrorAlert()
                    } else {
                        UserConfig.shared.checkInEvent(eventId: self.event.id)
                    }
                }
            })
            .addAction(title: "Cancelar", style: .cancel, handler: { (_, _) in })
            .build()
    
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Actions
    @IBAction func locationTouchUpInside(_ sender: Any) {
        MapManager.openMapsWithDestination(latitude: event.latitude, longitude: event.longitude, destinationName: event.title)
    }
    
    @IBAction func checkInButtonTouchUpInside(_ sender: Any) {
        if isCheckingDone {
            isCheckingDone = false
            UserConfig.shared.uncheckInEvent(eventId: event.id)
        } else {
            presentCheckinAlert()
        }
    }
    
    @IBAction func shareButtonTouchUpInside(_ sender: Any) {
        
        let shareItens: [Any] = [event.getShareableText()]
        
        let activityViewController = UIActivityViewController(activityItems: shareItens, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
}
