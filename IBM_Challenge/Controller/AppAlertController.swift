//
//  AppAlertController.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 28/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import UIKit


/// Helper class to manage and present UIAlertControllers
public class AppAlertController: NSObject {
    
    private var alertController: UIAlertController
    
    /// Mandatory textfields should not be empty before submitting
    private var mandatoryTextFields: [UITextField] = []
    private var submitAction: UIAlertAction?
    
    public init(title: String? = nil, message: String? = nil, preferredStyle: UIAlertController.Style) {
        self.alertController = UIAlertController(title: title, message: message, preferredStyle: preferredStyle)
    }
    
    
    /// Add an action to AlertView
    /// - Parameters:
    ///   - title: title string text
    ///   - style: style of button
    ///   - isSubmitAction: boolean indicating if this is the submit action of the alert
    ///   - handler: closure response
    /// - Returns: a reference to self object
    public func addAction(title: String, style: UIAlertAction.Style, isSubmitAction: Bool = false, handler: @escaping ((UIAlertAction?, UIAlertController) -> Void) = { _,_  in }) -> Self {
        
        let alertAction = UIAlertAction(title: title, style: style) { (action) in
            handler(action, self.alertController)
        }
        
        alertController.addAction(alertAction)
        if isSubmitAction {
            alertAction.isEnabled = false
            self.submitAction = alertAction
        }
        
        return self
    }
    
    
    /// Add a textfield to AlertView
    /// - Parameters:
    ///   - isMandatory: boolean indicating if the field is mandatory (text should not be empty)
    ///   - handler: closure response
    /// - Returns: a reference to self object
    public func addTextField(isMandatory: Bool = false, _ handler: @escaping ((UITextField?) -> Void) = { _ in }) -> Self {
        
        alertController.addTextField { (textField) in
            if isMandatory {
                self.mandatoryTextFields.append(textField)
                textField.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: .editingChanged)
            }
            handler(textField)
        }
        return self
    }

    public func build() -> UIAlertController {
        textFieldDidChange(UITextField())
        return alertController
    }
    
    /// Checks if there is any mandatory textfield empty, if so disable submit action button
    /// - Parameter textField: textfield with value being changed
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        submitAction?.isEnabled = true
        for textField in mandatoryTextFields {
            if let text = textField.text, text.isEmpty {
                submitAction?.isEnabled = false
            }
        }
    }
}
