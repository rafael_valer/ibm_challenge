//
//  MainViewController.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 26/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var refreshControl = UIRefreshControl()
    private var dataSource: EventsDataSource!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        loadData()
    }
    
    /// Initial setup view
    private func setupView() {
        
        // setup navigationBar
        title = "Eventos"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        // setup dataSource
        dataSource = EventsDataSource()
        
        // setup tableView
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.register(UINib(nibName: "EventTableViewCell", bundle: nil), forCellReuseIdentifier: "EventTableViewCell")
        tableView.separatorStyle = .none
        
        // setup refreshControl
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    /// Load data from api to populate list view
    private func loadData() {
        refreshControl.beginRefreshing()
        APIClient().getEvents { (events, error) in
            if let events = events {
                self.dataSource.updateEvents(events)
                self.tableView.reloadData()
            } else {
                // MARK: - TODO present some error message
            }
            self.refreshControl.endRefreshing()
        }
    }
}

// MARK: - Selectors
extension MainViewController {
    @objc func refresh(_ sender: AnyObject) {
       loadData()
    }
}

// MARK: - UITableViewDelegate
extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let event = dataSource.eventAtIndex(indexPath.row) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let eventDetailsVC = storyboard.instantiateViewController(withIdentifier: "EventDetailsViewController") as! EventDetailsViewController
            eventDetailsVC.event = event
            self.navigationController?.pushViewController(eventDetailsVC, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return EventTableViewCell.cellHeight
    }
}
