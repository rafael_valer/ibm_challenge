//
//  EventsDataSource.swift
//  IBM_Challenge
//
//  Created by Rafael Valer on 26/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import Foundation
import UIKit

class EventsDataSource: NSObject {
    
    private var events: [Event]
    
    override init() {
        self.events = []
    }
    
    func updateEvents(_ events: [Event]) {
        self.events = events
    }
    
    func eventAtIndex(_ index: Int) -> Event? {
        if(index < 0 || index >= events.count) { // index out of bounds
            return nil
        }
        return events[index]
    }
}

// MARK: - UITableViewDataSource
extension EventsDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if events.count == 0 {
            tableView.displayMessage("Nenhum evento foi encontrado, \npuxe para atualizar.")
        } else {
            tableView.backgroundView = nil
        }
        return events.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EventTableViewCell") as! EventTableViewCell
        cell.render(withEvent: events[indexPath.row])
        return cell
    }
}
