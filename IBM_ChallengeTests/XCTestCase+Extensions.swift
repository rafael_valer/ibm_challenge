//
//  XCTestCase+Extensions.swift
//  IBM_ChallengeTests
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//


import XCTest

extension XCTestCase {
    
    
    /// Helper function tests missing fields while decoding data to Decodable objects
    /// - Parameters:
    ///   - expectedKey: the key expected to fail while decoding
    ///   - decoding: the decoding class type
    ///   - data: data to be decoded
    func AssertThrowsKeyNotFound<T: Decodable>(_ expectedKey: String, decoding: T.Type, from data: Data) {
        XCTAssertThrowsError(try JSONDecoder().decode(decoding, from: data)) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertEqual(expectedKey, key.stringValue, "Expected missing key '\(key.stringValue)' to equal '\(expectedKey)'.")
            } else {
                XCTFail("Expected '.keyNotFound(\(expectedKey))' but got \(error)")
            }
        }
    }
}
