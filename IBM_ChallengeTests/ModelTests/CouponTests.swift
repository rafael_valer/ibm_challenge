//
//  CouponTests.swift
//  IBM_ChallengeTests
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import XCTest
@testable import IBM_Challenge

class CouponTests: XCTestCase {
    
    
    /// Testing an invalid JSON to be decoded to a Coupon object
    /// - Throws: error
    func testCouponWhenMissingId() throws {
        
        let couponWithMissingId = [
            "title":"Title",
            "discount":"90.2"
        ]
        
        let encoder = JSONEncoder()
        
        do {
            let jsonData = try encoder.encode(couponWithMissingId)
            AssertThrowsKeyNotFound("id", decoding: Coupon.self, from: jsonData)
        } catch let error {
            XCTFail("JSON serialization failed - \(error)")
        }
    }
}
