//
//  EventTests.swift
//  IBM_ChallengeTests
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import XCTest
@testable import IBM_Challenge

class EventTests: XCTestCase {
    
    /// Testing an invalid JSON to be decoded to an Event object
    /// - Throws: error
    func testEventWhenMissingId() throws {
        
        guard let url = Bundle.main.url(forResource: "single_event", withExtension: "json") else {
            XCTFail("Could not read contents of .json file")
            return
        }
        
        do {
            let data = try Data(contentsOf: url)
            AssertThrowsKeyNotFound("id", decoding: Event.self, from: data)
        } catch {
            XCTFail("Could not convert json to data - \(error.localizedDescription)")
        }
    }
}
