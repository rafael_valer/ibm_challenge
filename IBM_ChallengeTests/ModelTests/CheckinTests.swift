//
//  CheckinTests.swift
//  IBM_ChallengeTests
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import XCTest
@testable import IBM_Challenge

class CheckinTests: XCTestCase {
    
    
    /// Testing an invalid JSON to be decoded to a Checkin object
    /// - Throws: error
    func testCheckinWhenMissingEmail() throws {
        
        let checkinWithMissingEmail = [
            "eventId":"1",
            "name":"Rafael"
        ]
        
        let encoder = JSONEncoder()
        
        do {
            let jsonData = try encoder.encode(checkinWithMissingEmail)
            AssertThrowsKeyNotFound("email", decoding: Checkin.self, from: jsonData)
        } catch let error {
            XCTFail("JSON serialization failed - \(error)")
        }
    }
}
