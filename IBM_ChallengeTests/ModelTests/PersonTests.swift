//
//  PersonTests.swift
//  IBM_ChallengeTests
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import XCTest
@testable import IBM_Challenge

class PersonTests: XCTestCase {
    
    
    /// Testing an invalid JSON to be decoded to a Person object
    /// - Throws: error
    func testDecodingWhenMissingName() throws {
        
        let personWithMissingName = [
            "id":"1",
            "eventId":"1",
            "picture":"picture 1"
        ]
        
        let encoder = JSONEncoder()
        
        do {
            let jsonData = try encoder.encode(personWithMissingName)
            AssertThrowsKeyNotFound("name", decoding: Person.self, from: jsonData)
        } catch let error {
            XCTFail("JSON serialization failed - \(error)")
        }
    }
}
