//
//  APIClientTests.swift
//  IBM_ChallengeTests
//
//  Created by Rafael Valer on 25/06/20.
//  Copyright © 2020 Rafael Valer. All rights reserved.
//

import XCTest
@testable import IBM_Challenge

class APIClientTests: XCTestCase {
    
    
    /// Testing a valid request for the list of Events
    /// - Throws: error
    func testValidGetEvents() throws {
        
        let e = expectation(description: "Expecting the Events list")
        
        APIClient().getEvents { (events, error) in
            XCTAssertNotNil(events, "Events list nil")
            if let error = error {
                XCTAssertNil(error, "Get Events error not nil - \(error)")
            }
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15) { (error) in
            if let error = error {
                XCTFail("error: \(error.localizedDescription)")
            }
        }
    }
    
    /// Testing an invalid request for a specific event that does not exist
    /// - Throws: error
    func testInvalidGetEventWithId() throws {
        
        let e = expectation(description: "Expecting an Event with invalid Id")
        
        let invalidId = ""
        
        APIClient().getEvent(id: invalidId) { (event, error) in
            XCTAssertNil(event, "Event with invalid id found")
            if let error = error {
                XCTAssertNotNil(error, "Get Event error not nil - \(error)")
            }
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15) { (error) in
            if let error = error {
                XCTFail("error: \(error)")
            }
        }
    }
    
    /// Testing a valid checking in request for an event
    /// - Throws: error
    func testValidCheckIn() throws {
        
        let e = expectation(description: "Expecting valid CheckIn")
        
        let testCheckIn = Checkin(eventId: "1", name: "João da Silva", email: "joao@gklasd.com")
        
        APIClient().checkIn(checkin: testCheckIn) { (_, error, succeeded) in
            XCTAssertTrue(succeeded, "Check-in haven't succeeded")
            if let error = error {
                XCTAssertNil(error, "Check-In error not nil - \(error)")
            }
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15) { (error) in
            if let error = error {
                XCTFail("error: \(error.localizedDescription)")
            }
        }
    }
    
    
    /// Testing a valid image url fetching
    /// - Throws: error
    func testValidFecthImage() throws {
        
        guard let url = URL(string: "http://lproweb.procempa.com.br/pmpa/prefpoa/seda_news/usu_img/Papel%20de%20Parede.png") else {
            XCTFail("Could not create url")
            return
        }
        
        let e = expectation(description: "Expecting valid Image")
        
        APIClient().getImage(url: url) { (image, error) in
            XCTAssertNotNil(image, "Image is nil")
            if let error = error {
                XCTAssertNil(error, "Get image error not nil - \(error)")
            }
            e.fulfill()
        }
        
        waitForExpectations(timeout: 15) { (error) in
            if let error = error {
                XCTFail("error: \(error.localizedDescription)")
            }
        }
    }
}
